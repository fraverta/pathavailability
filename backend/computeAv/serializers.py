from rest_framework import serializers
from computeAv.models import AvComputeRequestModel

FACTORIZATION=0
MARKOV=1
SIMULATION=2

class ComputeAvReqSerializer(serializers.Serializer):
  graph = serializers.JSONField(binary=False)
  begin = serializers.CharField(required=True, allow_blank=False, max_length=50)
  end = serializers.CharField(required=True, allow_blank=False, max_length=50)
  method = serializers.IntegerField(required=True, min_value=0, max_value=2)
  sim_time = serializers.FloatField(required=False, min_value=0.0000019) #min value is  a minute expresses in year aproximately
  sim_error = serializers.FloatField(required=False, min_value=0.0)

  def create(self, validated_data):
    """
    Create and return a new `Snippet` instance, given the validated data.
    """
    print("ValidatedData: " + str(validated_data))

    if (validated_data["method"] == SIMULATION):
        if("sim_time" in validated_data and "sim_error" in validated_data):
            return AvComputeRequestModel.objects.create(percentage=0, result=-1.0, begin=validated_data["begin"],
                                                        end=validated_data["end"], method = validated_data["method"],
                                                        sim_time = validated_data["sim_time"],sim_error = validated_data["sim_error"],
                                                        graph=validated_data["graph"])
        else:
            if ("sim_time" in validated_data):
              return AvComputeRequestModel.objects.create(percentage=0, result=-1.0, begin=validated_data["begin"],
                                                      end=validated_data["end"], method=validated_data["method"],
                                                      sim_time=validated_data["sim_time"],
                                                      graph=validated_data["graph"])
            else:
                if ("sim_error" in validated_data):
                  return AvComputeRequestModel.objects.create(percentage=0, result=-1.0, begin=validated_data["begin"],
                                                              end=validated_data["end"], method=validated_data["method"],
                                                              sim_error=validated_data["sim_error"],
                                                              graph=validated_data["graph"])

    return AvComputeRequestModel.objects.create(percentage=0, result=-1.0, begin=validated_data["begin"],
                                                end=validated_data["end"], method=validated_data["method"],
                                                graph=validated_data["graph"])



  def update(self, instance, validated_data):
    """
    Update and return an existing `Snippet` instance, given the validated data.
    """
    instance.percentage = validated_data.get('percentage', instance.percentage)
    instance.result = validated_data.get('result', instance.result)
    instance.graph = validated_data.get('graph', instance.graph)
    instance.begin = validated_data.get('begin', instance.begin)
    instance.end = validated_data.get('end', instance.end)
    instance.save()
    return instance

class ResultAvReqSerializer(serializers.ModelSerializer):
    class Meta:
        model = AvComputeRequestModel
        fields = ('percentage','result',"err_description")
