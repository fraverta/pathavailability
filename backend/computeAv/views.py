# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from computeAv.models import AvComputeRequestModel
from computeAv.serializers import ComputeAvReqSerializer,ResultAvReqSerializer
from optlib.availability.diagrams import compute_min_paths,compute_expr
from rest_framework.parsers import JSONParser
from igraph import Graph
from optlib.availability.availability import *
from multiprocessing import Process
import sys
import time
from django import db
db.connections.close_all()
from threading import Lock
import _thread


# Maximum number of enqueue request.
MAX_REQUEST_ENQUEUE = 2

# Mamimum number of request that can be processed simultaneously. To avoid system fragmentation.
MAX_PROC = 2

#Lock to acess req_queue and proc. No read or write access must be make without a lock acquiere
lock = Lock()

#A queue of clients request
req_queue = []

#A list to control wich process are working in a request.
proc = [{"req_id":-1,"process":None} for i in range(MAX_PROC)]

#Amount of seconds in wich it checks req_queue to run requests.
DAEMON_SLEEP_TIME = 5

#We throw a thread that gives service to enqueued request when there are free resources
def daemom():
    while True:
        lock.acquire()
        checkAndRun()
        lock.release()
        time.sleep(DAEMON_SLEEP_TIME)

_thread.start_new_thread(daemom,())

@csrf_exempt
def snippet_list(request):
    """
    List all code snippets, or create a new snippet.
    """
    if request.method == 'GET':
        reqs = AvComputeRequestModel.objects.all()
        serializer = ComputeAvReqSerializer(reqs, many=True)
        return JsonResponse(serializer.data, safe=False)

    elif request.method == 'POST':
        try:
            data = JSONParser().parse(request)
            serializer = ComputeAvReqSerializer(data=data)
            if serializer.is_valid():
            # LOCK ACQUIRE BLOCK
                lock.acquire()
                if (len(req_queue) < MAX_REQUEST_ENQUEUE):
                    r = serializer.save()
                    req_queue.append(r)
                    # Check if there some free process
                    checkAndRun()
                    lock.release()
                    return JsonResponse({"id": r.id, "estimated_time": 3000}, status=201)
                # END LOCK ACQUIRE BLOCK
                else:
                    # END LOCK ACQUIRE BLOCK
                    lock.release()
                    #499 error code will be used as "Server busy to client Know about it"
                    return JsonResponse({}, status=499)

            #If serializer is invalid, return errors to client
            return JsonResponse(serializer.errors, status=400)

        except Exception as e:
            print("\n************Exception in  snippet_list************\n" +
                  str(e) +
                  "\n**************************************************\n")
            return JsonResponse(str(e), status=400)


@csrf_exempt
def snippet_detail(request, pk):
    """
    Retrieve, update or delete a code snippet.
    """
    try:
        avCompReq = AvComputeRequestModel.objects.get(pk=pk)
    except AvComputeRequestModel.DoesNotExist:
        return HttpResponse(status=404)

    if request.method == 'GET':
        serializer = ResultAvReqSerializer(avCompReq)
        return JsonResponse(serializer.data)

    elif request.method == 'PUT':
        data = JSONParser().parse(request)
        serializer = ComputeAvReqSerializer(avCompReq, data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data)
        return JsonResponse(serializer.errors, status=400)

    elif request.method == 'DELETE':
        cancelRequest(int(pk))
        return HttpResponse(status=204)


def computeAvailability(reqID, graph,  begin, end, method, sim_time, sim_error):
    '''
        Given the id of a AvComputeRequestModel, it method compute the availability between the nodes
        begin and end in the graph denoted by edgeList. After this computation, this method update the result in
        object with id reqID from AvComputeRequestModel.

        It designed by run as a Process. (Process(target=computeAvailability, args..))

        :param reqID: The pk (primary key) of the compute availability request (AvComputeRequestModel)
        :param edgeList: A list of json object {"in":in-value-str,"out":out-value-str, attr_list[0]: value,
                        ...,[attr_list[len(attr_list)] = value ]} as String
                if method = FACTORIZATION then attr_list = [AV]
                if method = TODO
        :param begin: The string identifier of the start edge.
        :param end: The string identifier of the end vertex.
        :param method: FACTORIZATION, MARKOV or SIMULATION.
    '''
    try:
        edges = [(e["from"], e["to"]) for e in graph["edges"]]
        #Check if begin and end are reachable if not result is 0
        if len(list(filter(lambda x: x[0] == begin,edges))) > 0 and len(list(filter(lambda x: x[1] == end,edges))) > 0:

            paths = compute_min_paths(edges, begin, end)
            expr=paths_to_expr(paths)

            if method==FACTORIZATION:
                avs = dict([(v["id"], float(v["a"])) for v in graph["nodes"]])
                result = factorization(expr,avs)

            elif method==MARKOV:
                mttfs = dict([(v["id"], float(v["mttf"]) if float(v["mttf"]) != -1.0 else None) for v in graph["nodes"]])
                mttrs = dict([(v["id"], float(v["mttr"]) if float(v["mttr"]) != -1.0 else None) for v in graph["nodes"]])
                result = markov(expr, mttfs, mttrs, {}, {})

            elif method == SIMULATION:
                mttfs = dict([(v["id"], float(v["mttf"]) if float(v["mttf"]) != -1.0 else None) for v in graph["nodes"]])
                mttrs = dict([(v["id"], float(v["mttr"]) if float(v["mttr"]) != -1.0 else None) for v in graph["nodes"]])
                delays = dict([(v["id"], float(v["delay"])) for v in graph["nodes"]])
                result = simulation(expr,mttfs,mttrs,delays,
                                    sim_time=sim_time if (sim_time != -1) else None,
                                    error_bound = sim_error if (sim_error != -1) else None)
        else:
            result = 0

        obj = AvComputeRequestModel.objects.get(pk=reqID)
        obj.percentage = 100
        obj.result = result
        obj.save()

    except Exception as e:
        print("\n************Exception in  computeAvailability************\n" +
              str(e) +
              "\n**************************************************\n")
        obj = AvComputeRequestModel.objects.get(pk=reqID)
        obj.percentage = 100
        obj.result = -2
        obj.err_description = str(e)
        obj.save()


#Check if there is request unprocessed and free process. If this is the case run the request in the queue top
def checkAndRun():
    #This method must be call with acquire lock
    if not lock.locked():
        raise Exception("checkAndRun lock must be acquired before call this method")

    while len(req_queue) > 0:
        for i in range(MAX_PROC):
            #if slot is free or process already finish process first enqueued request
            if proc[i]['process']==None or  not proc[i]['process'].is_alive():
                runRequest(req_queue.pop(0), i)
                break;

        #If there aren't free slots, break this loop
        if i+1 == MAX_PROC:
            break

def runRequest(req,freeSlot):
    #This method must be call with acquire lock
    if not lock.locked():
        raise Exception("runRequest lock must be acquired before call this method")

    try:
        db.connections.close_all()
        p = Process(target=computeAvailability, args=(req.id, req.graph, req.begin, req.end,
                                                      req.method, req.sim_time, req.sim_error))
        proc[freeSlot]['process'] = p
        proc[freeSlot]['req_id'] = req.id
        print(proc)
        p.start()

    except Exception as e:
        print("\n************Exception in  runRequest************\n" +
                                      str(e)+
              "\n**************************************************\n")

        obj = AvComputeRequestModel.objects.get(pk=req.id)
        obj.percentage = 100
        obj.result = -2
        obj.err_description = str(e)
        obj.save()
        proc[freeSlot]['process'] = None
        proc[freeSlot]['req_id'] = -1


'''
Kill process if request is running or delete it from queue in other case
'''
def cancelRequest(req_id):
    lock.acquire()
    found = False
    for p in proc:
        if p["req_id"] == req_id:
            if p["process"].is_alive:
                p["process"].terminate()
                p['process'] = None
                p['req_id'] = -1
                found = True

    if not found:
        for i in range(len(req_queue)):
            if req_queue[i].id == req_id:
                req_queue.pop(i)


    print(proc)
    print(req_queue)
    lock.release()

    obj = AvComputeRequestModel.objects.get(pk=req_id)
    obj.percentage = 100
    obj.result = -3
    obj.err_description = str("Process was cancel by user")
    obj.save()


'''
TODO: Define a log class for report this errors that leads with concurrences problems.
'''


