# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

class AvComputeRequestModel(models.Model):

  #Percentage of availability calculos advance
  percentage = models.BigIntegerField()

  #if percentage=100, result is the result of availability metric in graph from begin to end.
  #otherwise -1
  result = models.FloatField()

  #JSON representation of graph as list of edges.
  graph = models.TextField()

  method = models.IntegerField()

  #this fields take sence only if method="Simulation". And express the simulation time measure in year
  sim_time =  models.FloatField(default=-1.0)

  # this fields take sence only if method="Simulation". And express the error.
  sim_error = models.FloatField(default=-1.0)

  #start node for computing availability
  begin = models.CharField(max_length=50, blank=False)

  #end node for computing availability
  end = models.CharField(max_length=50, blank=False)

  #Description of errors, if it happend.
  #This field only has information if, result<-1
  err_description = models.TextField();

  class Meta:
    ordering = ('pk',)
