var app = angular.module('app');
app
  .controller('AppCtrl', function ($scope,$window) {
    $scope.counter = 0;
    $scope.tabs = [];
    $scope.currentTab = -1;

    var addTabFromJson = function (jsonText) {
      if($scope.currentTab > -1)
        $scope.tabs[$scope.currentTab].graph = graph.toJSON()    

      $scope.tabs.push({ title: 'schema ' + $scope.counter, graph: jsonText});
      $scope.counter++;
      $scope.tabs[$scope.tabs.length - 1].active = true;
      $scope.currentTab = $scope.tabs.length - 1;
      loadGraphInView();
    };

    var addEmptyTab = function(){
       gaux = new joint.dia.Graph;

      //element.attribute.app_type is 0,1,2 (0 iff av_begin, 1 iff av_end 2 otherwise(common cell))        
      avBegin = new joint.shapes.devs.Model({
          position: { x: 50, y: 50 },
          size: { width: 50, height: 50 },
          //inPorts: ['a'],
          outPorts: ['out'],
          ports: {
            groups: {
              'out': {
                attrs: {
                  '.port-body': {
                    fill: '#E74C3C'
                  }
                }
              }
              }
          },
          attrs: {
            '.label': { text: 'AvBegin', 'ref-x': .5, 'ref-y': .5 },
            rect: { fill: '#2ECC71' }
          },
          app_name: 'AvBegin',
          availability : 1.0,
          app_type: 0
           });
       gaux.addCell(avBegin);

      avEnd = new joint.shapes.devs.Model({
          position: { x: 200, y: 200 },
          size: { width: 50, height: 50 },
          inPorts: ['in'],        
          ports: {
            groups: {
              'in': {
                attrs: {
                  '.port-body': {
                    fill: '#16A085',
                    magnet: 'passive'
                  }
                }
              }
            }
          },
          attrs: {
            '.label': { text: 'AvEnd', 'ref-x': .5, 'ref-y': .5 },
            rect: { fill: '#2ECC71' }
          },
          app_name: 'AvEnd',
          availability : 1.0,
          app_type: 1
           });
       gaux.addCell(avEnd);

       addTabFromJson(gaux.toJSON())
    }

    var removeTab = function (event, index) {
      event.preventDefault();
      event.stopPropagation();
      $scope.tabs.splice(index, 1);

      if ($scope.tabs.length == 0){
        $scope.currentTab = -1;
        addEmptyTab();
      }else{

        if($scope.currentTab >= $scope.tabs.length)
          $scope.currentTab--;
       
        loadGraphInView()
      }
    };

    var tabClick = function (index) {
      $scope.tabs[$scope.currentTab].graph = graph.toJSON()
      $scope.currentTab = index;
      $scope.tabs[index].active = true;
      loadGraphInView();
    };    

    $scope.addTabFromJson = addTabFromJson;
    $scope.addEmptyTab = addEmptyTab;
    $scope.removeTab = removeTab;
    $scope.tabClick  = tabClick;


    function loadGraphInView(){
      $window.graph.fromJSON($scope.tabs[$scope.currentTab].graph);
      $window.scope.i = 0;
      vertexs = graph.getElements();
      for(v in vertexs){        
        if(vertexs[v].attributes.app_type === 2){
          current = vertexs[v];                    
          current.attributes.app_name = "V" + $window.scope.i++;  

          //add ports to generated elements
          current.addPort({group: 'top', attrs: { 'text': { text: '' } }});
          current.addPort({ group: 'bottom', attrs: { 'text': { text: '' } } });
          current.addPort({ group: 'left', attrs: { 'text': { text: '' } } });
          current.addPort({ group: 'right', attrs: { 'text': { text: '' } } });
        }       
      }
    }
  })
