/*
This controller implements the behavior of the console.
When a availability computing request is performed the 
coming steps should be followed:
	1) From JQuey call method $scope.canAddElement:
			This method evaluates if there is place for another 
			computing request. In such case it returns and index(refered to reqSlot) 
			in $scope.requests and follow with the step 2.
			Otherwise returns -1 and operation should be aborted(none action is nedeed). 
	
	2) JQuery sends a request to server, if all works fine 
			it must set $scope.requests[reqSlot]={expectedTime:fill with information from request, result:-1, iNum:-1, progress:0}


	3) JQuery calls method addElem. This method controlls(trhought callAtInterval method) progress bar and inform a result when it arrivals.
	
	4) JQuery publishes result to $scope.requests[reqSlot].result and it is read and reported by this method in asychronus mode.
		(this say in some moment in the future the result will be informed)

The behavior implemeted by callAtInterval is wait expectedTime and look for result it is computed report result. If not, 
do again the same. 

The user (real user) must free a slot by himself/herself clicking in trash icon.
*/


/*
Maximum amount of computing availability request in console.
*/
const MAX_REQUEST = 5;

//var app = angular.module('app', []);
	

angular.module('app').controller('ReqListCrt', function($scope,$compile,$interval) {
		$scope.consoleItem = 0;
		
		//{expectedTime,result,iNum,progress,err_description}
		$scope.requests = new Array(MAX_REQUEST);
		
		$scope.cleareableElements = [];
		
    $scope.count = 0;

		$scope.freeSlot = 
			function freeSlot(reqSlot){
				if(reqSlot >= 0 && reqSlot < MAX_REQUEST)
					$scope.requests[reqSlot] = null;
			}

		$scope.canAddReq = 
			function(id){ 
			/*
			this function returs -1 if there are MAX_REQUEST request running,
			otherwise returns a number between 0 and MAX_REQUEST wich indicates 
			the assigned slot in $scope.requests.																																		
			*/		
				var i;	
				for (i = 0; i < MAX_REQUEST; i++) {
			    	if( $scope.requests[i] == null){
						$scope.requests[i] = {"expectedTime":-1,"result":-1,"iNum":-1,"progress":0, "err_description":""}
						return i;
					}
				}
				return -1;
			}

		$scope.callAtInterval =
			//method which controlls progress bar and report result
			function(args){
				
				if ($scope.requests[args.reqSlot].iNum == 0){
					//if we wait expected time, we ask for result
					if($scope.requests[args.reqSlot].result >= 0){
						//if computing is finished: set progress var to 100% and inform result
						$scope.requests[args.reqSlot].progress = 100;
						//inform result 
  						var resultElem = angular.element(`<h5> <b> Availability = ${$scope.requests[args.reqSlot].result}</b> </h5>`);
  						angular.element(resultElem).appendTo(document.querySelector( args.id ));
						$scope.cleareableElements.push(args.id);
					}else{
								if ($scope.requests[args.reqSlot].result == -1){
									//if the result isn't arrives, reset progress var and create new interval
									$scope.requests[args.reqSlot].progress = 0;
									var time = ((5 * $scope.requests[args.reqSlot].expectedTime) / 100) + ((5 * $scope.requests[args.reqSlot].expectedTime) % 100)
									var count = ($scope.requests[args.reqSlot].expectedTime / time) + (($scope.requests[args.reqSlot].expectedTime % time !=0)? 1 : 0);
									$scope.requests[args.reqSlot].iNum = count;
									$interval(function(c){ $scope.callAtInterval({"id":args.id,"reqSlot":args.reqSlot}); }, time,count+1)
								}
								else{
								//$scope.requests[args.reqSlot].result < -1, an error has happend
									switch($scope.requests[args.reqSlot].result){
										case -2: 
												var resultElem = angular.element(`<h5> <b> Ups! The server is taking a lot. Try it later. </b> </h5>`);
												$scope.requests[args.reqSlot].progress = 100;
						  						angular.element(resultElem).appendTo(document.querySelector( args.id ));
												$scope.cleareableElements.push(args.id);
												break;
										case -3:
												var resultElem = angular.element(`<h5> <b> Ups! The server has experimented an unexpected error. Try it later. If error persists contact us.</b> </h5>`);
												$scope.requests[args.reqSlot].progress = 100;
						  						angular.element(resultElem).appendTo(document.querySelector( args.id ));
												$scope.cleareableElements.push(args.id);
												break;
										case -4:
												var resultElem = angular.element(`<h5> <b> Server is unreacheable. Check if you are in server network.</b> </h5>`);
												$scope.requests[args.reqSlot].progress = 100;
						  						angular.element(resultElem).appendTo(document.querySelector( args.id ));
												$scope.cleareableElements.push(args.id);
												break;

										case -5:
												var str = "Server has experimented an error: " + $scope.requests[args.reqSlot].err_description + "." 
												var resultElem = angular.element(`<h5> <b>` + str + `</b> </h5>`);
												$scope.requests[args.reqSlot].progress = 100;
						  						angular.element(resultElem).appendTo(document.querySelector( args.id ));
												$scope.cleareableElements.push(args.id);
												break;

										case -6:
												var str = "Process was cancel." 
												var resultElem = angular.element(`<h5> <b>` + str + `</b> </h5>`);
												$scope.requests[args.reqSlot].progress = 100;
						  						angular.element(resultElem).appendTo(document.querySelector( args.id ));
												$scope.cleareableElements.push(args.id);
												break;

									}
								}
							}
				}else{
							//if we don't wait expected time
							if($scope.requests[args.reqSlot].iNum > 1)
								//update progress bar + 5% (we take care to not fill the progress bar)
								$scope.requests[args.reqSlot].progress = $scope.requests[args.reqSlot].progress + 5;
							
							$scope.requests[args.reqSlot].iNum--	
						}						
				};

		//Compile and already added element to DOM. If it is a request it implements the progress bar behavior, otherwise its manage the delete bottom.
		$scope.addElem = 
			function(id,reqSlot){				
				$compile(id)($scope);	
				$scope.$apply();
				if(reqSlot != null){
					var time = ((5 * $scope.requests[reqSlot].expectedTime) / 100) + ((5 * $scope.requests[reqSlot].expectedTime) % 100)
					var count = ($scope.requests[reqSlot].expectedTime / time) + (($scope.requests[reqSlot].expectedTime % time !=0)? 1 : 0);
					$scope.requests[reqSlot].iNum = count;
					//call a method which controlls progress bar and report result
					$interval(function(c){ $scope.callAtInterval({"id":id,"reqSlot":reqSlot});}, time, count+1);
				}else
					$scope.cleareableElements.push(id);
		};

		//Remove an element from DOM. If it is a request it also free slot in requests array.
		$scope.removeElem = 
			function(id,reqSlot){
				var myEl = angular.element( document.querySelector( id ) );
				myEl.remove();
				$scope.cleareableElements.splice($scope.cleareableElements.indexOf(id),1)
				if(reqSlot!=null)
					$scope.freeSlot(reqSlot);
			}

		//Clear all element in #consoleList (if these were added using addElem method)
		$scope.clearList =
			function(){
				while($scope.cleareableElements.length > 0)
					$scope.removeElem($scope.cleareableElements[0]);

				
				var i;
				for(i in $scope.requests)
					//Free slot in request array, for deleted request from console.
					//A entry from request array were deleted in console only if $scope.requests[i].progress == 100
					if ($scope.requests[i] != null && $scope.requests[i].progress == 100)
						$scope.freeSlot(i);

				//clear list
				$scope.cleareableElements = [];
			}

});

