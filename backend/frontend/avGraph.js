//number of times that app will ask for a result to server
const AMOUNT_OF_GET_RESULT_REQ = 1000;

function genAndSendComputeAvReq(method,graph,iSlot, sim_time, sim_error){

	//$( "#console" ).append( "<p style=\"color:white\">Generating request...</p>" );
	sources  = graph.getSources();
	targets = graph.getSinks();
	source = getElement(sources, function(e){return e.attributes.app_type === 0 /*e es source*/});
	target = getElement(targets, function(e){return e.attributes.app_type === 1 /*e is target*/});

	var nodes = graph.getElements();
	graphRepre = {nodes:[],edges:[]}
	var label = 0;
	for (i in nodes){
		var v = nodes[i]
		switch(v.attributes.app_type ){
			case 0:
			case 1:
					graphRepre.nodes.push({id: v.attributes.app_name,a: 1, mttf: -1, mttr: -1, delay: 0.0})
					break;
			case 2:	
					//for security chose an unique name to each node in graph	
					v.attributes.app_name = "V" + i++	
					if (v.attributes.mttf != -1 && v.attributes.mttr != -1)
						availability = avFromMttfMttr(v.attributes.mttf,v.attributes.mttr)
					else
						availability = v.attributes.availability 

					graphRepre.nodes.push({id: v.attributes.app_name,
											a: availability, 
											mttf: v.attributes.mttf, 
											mttr: v.attributes.mttr / 8760,  //turn hours to years
											delay: v.attributes.delay / 8760 //turn hours to years
										}) 
					break;
		}
	}

	var edges = graph.getLinks()
	for (i in edges){ 
		var e = edges[i]
		//check if edge has end and source
		if (e.getSourceElement() != null && e.getTargetElement() != null){ 
			//Check if edge isn't already insert 
			isInserted = false
			for(j in graphRepre.edges)
				if( graphRepre.edges[j].from ==  e.getSourceElement().attributes.app_name 
					&& graphRepre.edges[j].to ==  e.getTargetElement().attributes.app_name){
					isInserted = true
					break;
				}		
			if (!isInserted)
				graphRepre.edges.push({from: e.getSourceElement().attributes.app_name,
								   to:  e.getTargetElement().attributes.app_name})
		}

	}

	//console.log("{\"begin\": O" + source.attributes.attrs[".label"].text + " ,\"end\": I" + target.attributes.attrs[".label"].text + " ,\"graph\": [" + graphStr + "]}");
	data = {begin: source.attributes.app_name,
			end: target.attributes.app_name, 
			method: method, graph: graphRepre}
	


	if(method == SIMULATION){
		if( sim_time != "")
			data["sim_time"] = parseFloat(sim_time)
		if(sim_error != "")
			data["sim_error"] = parseFloat(sim_error)
	}

	//gen http request
	//console.log($.get("http://127.0.0.1:8000/snippets/",dataType="json"));
	sendComputingReq("snippets/",iSlot,data)
	console.log(JSON.stringify(data));
	//$( "#console" ).append( "<p style=\"color:white\">Sending Request...</p>" );
}
	

function sendComputingReq(url,iSlot,data){
	$.post(url,
		   JSON.stringify(data),
		   function(result){
				console.log(result.estimated_time);
				var id = result.id;
				createReqInConsole(iSlot,result.estimated_time,id);					
				setTimeout(function(){askForResult(id,iSlot,result.estimated_time,AMOUNT_OF_GET_RESULT_REQ)}, result.estimated_time);
			}		   
		  ).fail(function(response){
		  		console.log("Fail sendComputingReq")
				//free slot
				var $scope = angular.element('#consoleList').scope();
		  		$scope.freeSlot(iSlot)
        		if(response.status == 0){
					createErrInConsole("Server unreachable",
					"Check if you are in the server network." )
        		}else
    				switch (response.status){
	    				case 499:
								createErrInConsole("Server is busy", "Server has a lot of work to do. Please wait same minutes and try again.")
								break
	        			case 500:
	        					createErrInConsole("Internal server error","Server has experimented an error. Please wait and try again. If the error persist contact us.")
	        					break
	        			default:
								createErrInConsole(response.status.toString(),response.statusText)
								break
				}
			})}

function askForResult(id,iSlot,time_to_wait,times){
	$.get("snippets/"+id+"/",
		  function f(res){computeAvResult(res,id,iSlot,time_to_wait,--times)},
		  dataType="json")
	.fail(function(response){
  		console.log("Fail askForResult")
		//report error
		var $scope = angular.element('#consoleList').scope();
		if( response.status == 500)
			$scope.requests[iSlot].result = -3; // server has experimented an unexpected error
		if( response.status == 0)
			$scope.requests[iSlot].result = -4; //server unreachable
	})
};

function computeAvResult(res,id,iSlot,time_to_wait,times){
	console.log(id,res,times)
	var avRes = res.result;

	switch(avRes){
		case -1:
				//wait and ask for result again
				if(times > 0)
					setTimeout(function(){askForResult(id,iSlot,time_to_wait,times)}, time_to_wait);			
				else{
					//report error
					var $scope = angular.element('#consoleList').scope();
					$scope.requests[iSlot].result = -2;
				}
				break;
		case -2: 
				//A server side error happend. Repor it
				var $scope = angular.element('#consoleList').scope();
				$scope.requests[iSlot].result = -5;
				$scope.requests[iSlot].err_description = res.err_description
				break;
		
		case -3:
				//Process was cancelled by user
				var $scope = angular.element('#consoleList').scope();
				$scope.requests[iSlot].result = -6;
				$scope.requests[iSlot].err_description = res.err_description
				break;


		default: 
				//Result were computed, so inform it.
				var $scope = angular.element('#consoleList').scope();
				$scope.requests[iSlot].result = avRes;
				break;
	}	
}

function getElement(arr,cfun){
	console.log(arr)
	for(e in arr){
        if (cfun(arr[e]))
			return arr[e];
		console.log(e);
	}
	return null;
}

	
function createReqInConsole(iSlot,expectedTime,req_id){
		var item = [];
		var $scope = angular.element('#consoleList').scope();
		var id = "iCon" + $scope.consoleItem++;
		item.push(`<div style="margin: 10px 5px 10px 35px" id = ${id} class="list-group-item">`)
		item.push('<h4 class="list-group-item-heading"> Running <a href="#">model</a></h4>'); 
		item.push('<div style="position: relative; top: 10px; left: 50px;width:80%" class="progress">');
		item.push(`<div class="progress-bar progress-bar-striped active" 
					ng-class="(requests[${iSlot}].result == -1)? 'progress-bar-info' : 
							(requests[${iSlot}].result >= 0)? 'progress-bar-success': 'progress-bar-danger'"
					role="progressbar" 	
					aria-valuenow={{requests[${iSlot}].progress}} 
					aria-valuemin="0" aria-valuemax="100" 
					style="width:{{requests[${iSlot}].progress}}%">
					</div></div>`);	

		item.push(`<button title="Delete entry" type="button" style="position: absolute;top: 10px;right: 10px;" href="#" class="btn btn-default" ng-show="requests[${iSlot}].progress == 100"  ng-click=removeElem("#${id}",${iSlot})>`); 
		item.push('<span  class="glyphicon glyphicon-trash"></span></button>');
		item.push(`<button title="Cancel running" id="reqCancelBtn-${req_id}" type="button" style="position: absolute;top: 10px;right: 10px;" href="#" class="btn btn-default"  ng-show="requests[${iSlot}].progress != 100" onclick="cancelRequest(${req_id})">`);
		item.push('<span   class="lg glyphicon glyphicon-remove-sign"></span></button>');
		item.push('</div>')			
		item = item.join("");
		$("#consoleList").prepend(item);
		$scope.requests[iSlot] = {"expectedTime":expectedTime,"result":-1,"iNum":0,"progress":0};
		$scope.addElem("#" + id,iSlot);
}

/**
Try to create a request message if it fails report the error otherwise a request is sended to server and a result is reported
*/
function computeAvReq(graph, method, sim_time, sim_error){
	var $scope = angular.element('#consoleList').scope();
	var iSlot = $scope.canAddReq();
		
	if(iSlot == -1){
		//report error
		createErrInConsole("too many computing request","We can't create another request, please delete one finished request from console")	
		//console.log("Can't create another request, please delete one");
	}else{
		//generate  and send request
		if (sanitize_graph(graph,method))
			genAndSendComputeAvReq(method, graph, iSlot, sim_time, sim_error);
	}
}

/*
Create an error message in console
**/
function createErrInConsole(errName,errDescription){
		var item = [];
		var $scope = angular.element('#consoleList').scope();
		var id = "iCon" + $scope.consoleItem++;
		item.push(`<div id = ${id} style="margin: 10px 5px 10px 35px" class="list-group-item">`)
	 	item.push(`<h4 class="list-group-item-heading"> <spam style="color:red;font-weight: bold;">Error </spam> ${errName} <a href="#">at running model</a>`); 
    	item.push(`<p class="list-group-item-text" style="margin:20px 0px 0px 50px;width:80%;text-align: justify;text-justify: inter-word;">${errDescription}</p>`)
    	item.push(`<button type="button" style="position: absolute;top: 10px;right: 10px;" href="#" class="btn btn-default" ng-click=removeElem("#${id}")>`); 
    	item.push('<span  class="glyphicon glyphicon-trash"></span></button>');
		item.push('</div>')
		item = item.join("");
		$("#consoleList").prepend(item);
		$scope.addElem("#" + id);
}


function avFromMttfMttr(mttf,mttr){
	mttr = mttr / 8760 //turn hours to years
	mtbf = mttf + mttr
    return mttf / mtbf
}



//Check if graph contains requeried fields to do computation 
function sanitize_graph(graph,method){
	//Check if all nodes have a defined mttf and mttr
	if(method == 1 || method == 2)
		var nodes = graph.getElements();
		for(n in nodes){
			if (nodes[n].attributes.app_type == 2 && nodes[n].attributes.mttf <= 0){
				createErrInConsole("Mttf argument is not declared",
									"Mttf has not been declared for element " + nodes[n].attributes.attrs[".label"].text + "." )
				return false;
			}
			else if (nodes[n].attributes.app_type == 2 && nodes[n].attributes.mttr <= 0){
					createErrInConsole("Mttr argument is not declared",
					"Mttr has not been declared for element " + nodes[n].attributes.attrs[".label"].text + "." )
					return false;

			}
		}
		return true;
}

function cancelRequest(id){
	$.ajax({
    url: "snippets/"+id+"/",
    method: 'DELETE',
    //contentType: 'application/json',
    success: function(result) {
    	console.log("Deleted")
    	$("#reqCancelBtn-" + id).attr("disabled",true);
    },
    error: function(request,msg,error) {
        console.log("Non Deleted")
    }
	});

	/*	
	.fail(function(response){
  		console.log("Fail askForResult")
		//report error
		var $scope = angular.element('#consoleList').scope();
		if( response.status == 500)
			$scope.requests[iSlot].result = -3; // server has experimented an unexpected error
		if( response.status == 0)
			$scope.requests[iSlot].result = -4; //server unreachable
	})
	*/
}