joint.shapes.devs.AppModel = joint.shapes.devs.Model.extend({

  markup: '<g class="rotatable"><g class="scalable"><rect class="body"/></g><text class="label"/><g class="port"/><g class="port"/><title /></g>',
  defaults: joint.util.deepSupplement({

    type: 'devs.AppModel',
    attrs: {		
      '.body': {
        r: 50,
        cx: 50,
        stroke: 'blue',
        fill: 'green'
      },
      '.label': {
        text: "",
        'ref-y': 0.5,
        'y-alignment': 'middle'
      },
    }

  }, joint.shapes.devs.Model.prototype.defaults)
});

joint.shapes.devs.AppModelView = joint.shapes.devs.ModelView;

function createGraphElement(x,y,width,height,name){
	var rect = new joint.shapes.devs.AppModel({
		position: {
			x: x,
			y: y
		},
	attrs: {
				 title: {html:
							name +
							'<br>   Availability = ' + 1 +
							'<br>   MTTF = undefined' +
    						'<br>   MTTR = undefined' +
							'<br>   Delay = 0.0' 
				 		},
		    '.label': {
		      text: name
				  }
				},
		size: {
			width: width,
			height: height
		},
		 ports: {
			    groups: {
			        'top': {
			            // port position definition
			            position: 'top',
									label: {
											position: {
													name : 'top',
													args: {
															x: 0,
															y: -12,
															angle: 0,
															offset:0,
															attrs: {}
													}
											},
											markup:'<text class="joint-port-label" fill="#000000"/>'
									},
									markup:	'<circle class="port-body" r="10" fill="#E74C3C" stroke="#000" port="out" port-group="top" magnet="true"/>'
			        },

			        'bottom': {
			            // port position definition
			            position: 'bottom',
									label: {
											position: {
													name : 'bottom',
													args: {
															x: 0,
															y: 13,
															angle: 0,
															offset:0,
															attrs: {}
													}
											},
											markup:'<text class="joint-port-label" fill="#000000"/>'
									},
									markup:	'<circle class="port-body" r="10" fill="#E74C3C" stroke="#000" port="out" port-group="bottom" magnet="true"/>'
			        },

			        'right': {
			            // port position definition
			            position: 'right',
									label: {
											position: {
													name : 'right',
													args: {
															x: 12,
															y: 0,
															angle: 0,
															offset:0,
															attrs: {}
													}
											},
											markup:'<text class="joint-port-label" fill="#000000"/>'
									},
																	attr:{text:{text:'port1'}},
									markup:	'<circle class="port-body" r="10" fill="#E74C3C" stroke="#000" port="out" port-group="right" magnet="true"/>'
			        },

			        'left': {
			            // port position definition
			            position: 'left',
									label: {
											position: {
													name : 'left',
													args: {
															x: -12,
															y: 0,
															angle: 0,
															offset:0,
															attrs: {}
													}
											},
											markup:'<text class="joint-port-label" fill="#000000"/>'
									},
									markup:	'<circle class="port-body" r="10" fill="#E74C3C" stroke="#000" port="out" port-group="left" magnet="true"/>'
			        }
					}
		},
	  	app_name: name,
	  	availability : 1.0,
	  	mttf: -1,
	  	mttr: -1,
	  	delay: 0,
	  	app_type: 2
	});


	rect.addPort({group: 'top', attrs: { 'text': { text: '' } }});
	rect.addPort({ group: 'bottom', attrs: { 'text': { text: '' } } });
	rect.addPort({ group: 'left', attrs: { 'text': { text: '' } } });
	rect.addPort({ group: 'right', attrs: { 'text': { text: '' } } });
	
	return rect;
}
