//compute methods
const FACTORIZATION = 0
const MARKOV = 1
const SIMULATION = 2

		var scope;
		 $(function() {	
			scope.createGraphElement = createGraphElement;
		  var currentCell;
		  graph = new joint.dia.Graph;
		  paper = new joint.dia.Paper({
		  el: $('#paper-link-snapping'),
		  width: "300vw", height: "300vh", gridSize: 1,
		  model: graph,
		  defaultLink: new joint.dia.Link({
		      attrs: { '.marker-target': { d: 'M 10 0 L 0 5 L 10 10 z' } }
		  }),
		  validateConnection: function(cellViewS, magnetS, cellViewT, magnetT, end, linkView) {
		      sModel  = cellViewS.model;
			  tModel = cellViewT.model;
			  // Prevent loop linking and Av-Begin in connections
		      return (magnetS !== magnetT && 
					  cellViewS != cellViewT && 
					  cellViewT.model.attributes.app_type != 0 
					  //not already conected nodes
					  //&& !graph.isNeighbor(sModel,tModel,{"outbound":true})
					 );
		  },
			  
		  // Enable link snapping within 75px lookup radius
		  snapLinks: { radius: 75 }
	});

/*****************************************************************
++++++++++++++++++++++++Context Menu behavior+++++++++++++++++++++
******************************************************************/
	$("#contextMenu").on("click", "a", function() {		
		$("#contextMenu").hide();
	 });

	paper.on('cell:contextmenu ', function(element) {
      if(element && element.model instanceof joint.dia.Element){
	      if(element.model.attributes.app_type > 1){
			  $("#contextMenu").css({
				 display: "block",
				 left: event.pageX,
				 top: event.pageY	      	 
			  });
			  currentCell = element.model;	   
		  }
	  }
	});

	//*****Edit Node behavior*****

	$("#li-edit-component").on("click",function(e) {     
	 	e.preventDefault();  	   
        //show current cells values
        $('#edit_component_label').val(currentCell.attributes.attrs[".label"].text );           

        //show modal window
        $('#modal-edit-component').modal('show');        
    });

        //Button modal ok behavior
    $("#modal-edit-btnOk").click(function(e){
    	 e.preventDefault();
		if($("#modal-edit-component-form").valid()){
			$('#modal-edit-component').modal('hide'); 
			currentCell.attributes.attrs[".label"].text = $("#edit_component_label").val()
			currentCell.attr('title/html',
							'<spam style="text-align:left">' + currentCell.attributes.attrs[".label"].text + '</spam>'+ 
							'<br>   Availability = ' + currentCell.attributes.availability +
							'<br>   MTTF = ' + ((currentCell.attributes.mttf== -1)? "undefined" : currentCell.attributes.mttf) +
    						'<br>   MTTR = ' + ((currentCell.attributes.mttr == -1)? "undefined": currentCell.attributes.mttr) +
							'<br>   Delay = ' + currentCell.attributes.delay
    						);
		}
    });


	$("#modal-edit-component-form").validate({
			rules: {
				edit_component_label: {
					required: false,
					minlength:1
				}
			},
			messages: {
				edit_component_label: {
					minlength: "The label can not be empty",
				}
			}           
    	});


/*****************************************************************
+++++++++++++++++++++ End Context Menu behavior ++++++++++++++++++
******************************************************************/

	paper.on('cell:pointerdblclick', function(element) {		 
        if(element && element.model instanceof joint.dia.Element && element.model.attributes.app_type > 1){
            currentCell = element.model;	   
            //show current cells values
            $('#avModalInput').val(currentCell.attributes.availability);           
        	$('#mttf').val((currentCell.attributes.mttf == -1)? "": parseFloat(currentCell.attributes.mttf)) ;
        	$('#mttr').val((currentCell.attributes.mttr == -1)? "":parseFloat(currentCell.attributes.mttr));           
            $('#delay').val(currentCell.attributes.delay);

            //show modal window
            $('#myModalHorizontal').modal('show');
        }
	});


	paper.on('blank:pointerdown', function(element) {$("#contextMenu").hide();});
	paper.on('cell:pointerdown', function(element) {console.log(element.model.attributes.availability);$("#contextMenu").hide();});
	paper.on('link:pointerdown', function(element) {$("#contextMenu").hide();});

	angular.element("#tabBar").scope().addEmptyTab();
	angular.element("#tabBar").scope().$apply();
	
	$("#li-remove").on("click",function(e) {
		 e.preventDefault();
    	console.log("remove element");
    	console.log(currentCell);
    	currentCell.remove();
    });

	$("#sidebar-toggle-btn").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
        $("#sidebar-toggle-btn").addClass("hide");
    });

    $("#side-bar-close-btn").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
		$("#sidebar-toggle-btn").removeClass("hide");
    });

    $("#console-close-btn").click(function(e) {
        e.preventDefault();
        $("#console").toggleClass("toggled");
        $("#console-toggle-btn").removeClass("hide");
		$("#console-close-btn").addClass("hide");
		$("#console-clear-btn").addClass("hide");
        $("#container").addClass("All-Window-Paper");
    });

    $("#console-toggle-btn").click(function(e) {
        e.preventDefault();
        $("#console").toggleClass("toggled");
        $("#console-toggle-btn").addClass("hide");
		$("#console-close-btn").removeClass("hide");
		$("#console-clear-btn").removeClass("hide");
        $("#container").removeClass("All-Window-Paper");
    });

    $("#compute_method").on("change", function(e){
    	if ($("#compute_method").val() == SIMULATION)
    		 $("#simulation-params").show();
    	else
    		$("#simulation-params").hide();
    });
      
/**************************************************************
--------------------MODAL FORM BEHAVIOR-----------------------
**************************************************************/

	$("#newModalForm").validate({
			rules: {
				avModalInput: {
					required: false,
					min: 0,
					max: 1
				}, 
				mttf: {
					required: false,
					min: 0.000000277777777777778
				},
				mttr: {
					required: false,
					min: 0.000000277777777777778
				}
			},
			messages: {
				avModalInput: {
					min: "The availability must be a real value between 0 and 1",
					max: "The availability must be a real value between 0 and 1"
				}
			}           
    	});
    
    //Button modal ok behavior
    $("#btnModalOk").click(function(e){
		if($("#newModalForm").valid()){
			$('#myModalHorizontal').modal('hide'); 
			currentCell.attributes.availability = $("#avModalInput").val() === ""? currentCell.attributes.availability : $("#avModalInput").val();
			currentCell.attributes.mttf= $("#mttf").val() === "" ? -1 : parseFloat($("#mttf").val()) ;
			currentCell.attributes.mttr= $("#mttr").val() === "" ? -1 : parseFloat($("#mttr").val()) ;
			currentCell.attributes.delay= $("#delay").val() === "" ? currentCell.attributes.delay : parseFloat($("#delay").val()) ;
			//currentCell.attributes.attrs.title.text = "Availability = " + currentCell.attributes.availability;
			currentCell.attr('title/html',
							'<spam style="text-align:left">' + currentCell.attributes.attrs[".label"].text + '</spam>'+ 
							'<br>   Availability = ' + currentCell.attributes.availability +
							'<br>   MTTF = ' + ((currentCell.attributes.mttf== -1)? "undefined" : currentCell.attributes.mttf) +
    						'<br>   MTTR = ' + ((currentCell.attributes.mttr == -1)? "undefined": currentCell.attributes.mttr) +
							'<br>   Delay = ' + currentCell.attributes.delay
    						);
		}
    });
			 
	$("#newModalForm").keypress(function (e) {
        if ((e.which && e.which == 13) || (e.keyCode && e.keyCode == 13)) {
            $('#btnModalOk').click();
            return false;
        } else {
            return true;
        }
    });
			 
			 
/**************************************************************
-----------------*END* MODAL FORM BEHAVIOR--------------------
**************************************************************/
			 
	$("#btnRun").click(function(e){
        //show modal window
        e.preventDefault();
    	$('#choseMethodModal').modal('show');
    	$("#sim_time").val("");
    	$("#sim_error").val("");
	})
    
    //Button modal ok behavior
    $("#btnCmpMethodModalOk").click(function(e){
		e.preventDefault();
		var sim_time;
		var sim_error;

		method = parseInt($("#compute_method").val())
		if(method == SIMULATION){
			sim_time = $("#sim_time").val()
			sim_error = $("#sim_error").val()
		}
		$('#choseMethodModal').modal('hide');
		computeAvReq(graph, method, sim_time, sim_error);

		//show console if it is hidden
		if(!$("#console-toggle-btn").hasClass("hide"))
			$("#console-toggle-btn").click()
    });

	
 	$("#btn-1").click(function(e){
		e.preventDefault();
	});
	
	 $("#btn-models").click(function(e){
		e.preventDefault();
	});
		 
			 
			 
 	$("#btn-save").click(function(e){
		e.preventDefault();
		str = JSON.stringify(graph.toJSON());
		var blob = new Blob([str], {type: "text/plain;charset=utf-8"});
		saveAs(blob, "model.txt");
	})
		
	//$("#logo").css('opacity','0');			 
	function handleFileSelect(evt) {
		var files = evt.target.files; // FileList object

        var reader = new FileReader();
        reader.onload = function(){
         	var text = reader.result;
			console.log(text);
			var textJSON = JSON.parse(text);
			angular.element("#tabBar").scope().addTabFromJson(textJSON);
			angular.element("#tabBar").scope().$apply();
		}
		// Read in the image file as a data URL.
      reader.readAsText(files[0],"text/plain;charset=utf-8");	 
	};

			 
  	$("#logo").on('change', handleFileSelect);
	$("#btn-load").click(function(e){		
		e.preventDefault();
		$("#logo").val('');
		$("#logo").trigger('click');
		
		//var jsonString = JSON.stringify(graph)
		//graph.fromJSON(JSON.parse(jsonString))
	});


	$("#compute-method-form").validate({
			rules: {
				sim_time: {
					required: false,
					min: 0.0000019
				}, 
				sim_error: {
					required: false,
					min: 0
				}
			}        
    	});

			 
/**************************************************************
------------------- CONSOLE BEHAVIOR  ----------------------
**************************************************************/                                


		 
/**************************************************************
-----------------*END* CONSOLE BEHAVIOR--------------------
**************************************************************/
});
	

	/*
	Drag and drop functions
	*/

	function allowDrop(ev) {
	    ev.preventDefault();
	}

	function drag(ev) {
	    ev.dataTransfer.setData("text", "atom");
	}

	function dragStartNode(ev) {
	    ev.dataTransfer.setData("text", "start");
	}

	function dragEndNode(ev) {
	    ev.dataTransfer.setData("text", "end");
	}




	function drop(ev) {
		ev.preventDefault();
		scope.addNodeDrop(ev);
	}



