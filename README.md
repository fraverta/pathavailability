
# PATHAVAILABILITY

**PathAvailability** is a tool to graphycally model a network and compute the network's availability (i.e the probability of a network is working properly at a specific time).

This software was used for the engineers of the company's network infrastructure team to solve practical exercises during the training.

**Brief video for an overview of the application:** https://youtu.be/4T2pkLOMh7Q
